![](img/logo.png)

# Projekt Milionerzy

### Akademia Nauk Stosowanych w Tarnowie

### **Katedra Informatyki**


Autorzy:
### Mikołaj Bryndal

### Szymon Grzeszczuk

Przedmiot: **Narzędzia i środowiska programistyczne**

Prowadzący: **Tomasz Gądek**

Kierunek: **Informatyka**

Semestr: **Letni**

Rok: **2**



Grupy: **PN1** (Mikołaj Bryndal) i **PN2** (Szymon Grzeszczuk)

## **Opis Funkcjonalny**

Po uruchomieniu aplikacji, zostaniemy przywitani komunikatem, który
wyjaśnia na czym będzie polegać nasza gra.

![](img/1.png)

Program, dla każdego etapu losuje pytania dla gracza i wyświetla
prawidłową odpowiedź po odpowiedzi gracza. W przypadku poprawnej
odpowiedzi, realizowane jest przejście do kolejnego pytania lub
wyświetlenie komunikatu o zwycięstwie bądź przegranej gracza.

Dodatkowo zaimplementowana została obsługę trzech kół ratunkowych
mających na celu ułatwienie graczowi wygranie głównej nagrody miliona
złotych! Są to:

- Telefon do przyjaciela 
    >Do pytania numer 5 przyjaciel zawsze podaje
     prawidłową odpowiedź, a dla kolejnych pytań losowana jest
     podpowiedź - 1 z 4 odpowiedzi.

- 50 na 50 
    >Usuwane zostają 2 nieprawidłowe odpowiedzi.

- Pytanie do publiczności
    >Dla prawidłowej odpowiedzi losowana jest
    wartość z przedziału 50% - 100%, po otrzymaniu wyniku ustalana
    jest wartość dla pozostałych odpowiedzi według własnego algorytmu.

<!-- -->

-   Dla jednej gry można wykorzystać maksymalnie 3 koła ratunkowe

Przykład jednego z pytań + użycie koła ratunkowego 50/50 i wybranie
poprawnej odpowiedzi

![](img/2.png)

![](img/3.png)

![](img/4.png)

Przykład wybrania błędnej odpowiedzi

![](img/5.png)

**Narzędzia użyte do realizacji projektu**

- IntelliJ IDEA IDE

- Java

- System kontroli wersji Git

- Bitbucket

## **Podział pracy**
  ----------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------
### Mikołaj Bryndal
-Główna klasa kontrolująca grę - metody sprawdzające input użytkownika, użycie kół ratunkowych, wypisywanie danych w konsoli

-Enum LifeLine reprezentujący wszystkie możliwe koła ratunkowe i ich reprezentacje tekstowe

-Interfejs View określający funkcjonalność, którą implementują wszystkie widoki.

-Klasa MainView odpowiadająca za tekstową implementacje interfejsu View

-Pomoc w powstawaniu klasy Question

-JavaDoc

### Szymon Grzeszczuk
-Główna klasa kontrolująca grę - metody wypisujace numer rundy i nagrodę, użycie koła ratunkowego 50/50, losowanie pytań, logika i algorytm dla kół ratunkowych

-Poprawne uzyskanie koła ratunkowego z danych wejściowych użytkownika

-Rozszerzenie klasy View, więcej metod

-Pomoc z ładowaniem pytań do odpowiedniego modelu danych

-JavaDoc


